const express = require('express');
const app = express();
const md5 = require ("md5");
const port = 3000;
const routeUsers = require("./Users/routeUsers");
const {join} = require('path');

app.use('/chapter3', express.static(join(__dirname, 'Page Chapter 3')));
app.use('/chapter4', express.static(join(__dirname, 'Page Chapter 4')));

app.get('/chapter3', (req, res) => {
  res.sendFile(join(__dirname, 'Page Chapter 3', 'cobaantiga.html'));
});

app.get('/chapter4', (req, res) => {
  res.sendFile(join(__dirname, 'Page Chapter 4', 'cobaanempat.html'));
});

app.use(express.json());
app.use("/users", routeUsers);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})




