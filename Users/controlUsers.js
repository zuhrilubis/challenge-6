const modelUsers = require("./modelUsers");

class controlUsers {
  getAllUsers = async (req, res) => {
    const allUsers = await modelUsers.getAllUsers();
    return res.json(allUsers);
  };

  getOneUser = async (req, res) => {
    const { userId } = req.params;
    const userData = await modelUsers.getOneUser(userId);

    if (userData) {
      return res.json(userData);
    } else {
      res.statusCode = 404;
      return res.json({ message: `User id un identified : ${userId}` });
    }
  };

  storeUserBio = async (req, res) => {
    const dataBio = req.body;
    if (dataBio.fullname === undefined || dataBio.fullname === "") {
      res.statusCode = 404;
      return res.json({ message: "write your full name please" });
    }
    if (dataBio.Address === undefined || dataBio.Address === "") {
      res.statusCode = 404;
      return res.json({ message: "write your address please" });
    }
    if (dataBio.PhoneNumber === undefined || dataBio.PhoneNumber === "") {
      res.statusCode = 404;
      return res.json({ message: "write your phone number please" });
    }

    if (dataBio.User_id === undefined || dataBio.User_id === "") {
      res.statusCode = 404;
      return res.json({ message: "write your ID please" });
    }

    const usedBio = await modelUsers.isBioIdentified(dataBio);
    if (usedBio) {
      res.statusCode = 404;
      return res.json({ message: "Biodata already exist" });
    }

    const unUsedData = await modelUsers.isUserUnIdentified(dataBio);
    if (!unUsedData) {
      res.statusCode = 404;
      return res.json({ message: "user Id is unregistered" });
    }

    modelUsers.storeUserBio(dataBio);

    res.json({ message: "new biodata is being added!" });
  };

  updateUserBio = async (req, res) => {
    const { userId } = req.params;
    const { fullname, Address, PhoneNumber } = req.body;

    const UpdateBio = await modelUsers.isUpdateBioUndiefined(userId);
    if (!UpdateBio) {
      res.statusCode = 404;
      return res.json({ message: "user Id is undefined or Biodata not exist" });
    }

    modelUsers.updateUserBio(userId, fullname, PhoneNumber, Address);

    return res.json({ message: `Biodata user Id: ${userId} is being updated` });

  };

  storeNewUsers = async (req, res) => {
    const daftarData = req.body;
    if (daftarData.username === undefined || daftarData.username === "") {
      res.statusCode = 404;
      return res.json({ message: "username is unavailable" });
    }
    if (daftarData.email === undefined || daftarData.email === "") {
      res.statusCode = 404;
      return res.json({ message: "email is unavailable" });
    }
    if (daftarData.password === undefined || daftarData.password === "") {
      res.statusCode = 404;
      return res.json({ message: "password is unavailable" });
    }

    const usedData = await modelUsers.isUserIdentified(daftarData);
    if (usedData) {
      res.statusCode = 404;
      return res.json({ message: "username or email already exist" });
    }

    modelUsers.storeNewUsers(daftarData);

    res.json({ message: "new user is being added!" });
  };

  verifyLogin = async (req, res) => {
    const { username, password } = req.body;
    const dataLogin = await modelUsers.verifyLogin(username, password);

    if (dataLogin) {
      return res.json(dataLogin);
    } else {
      res.statusCode = 404;
      return res.json({ message: "user not identified!" });
    }
  };

  storeGameHistory = async (req, res) => {
    const daftarGame = req.body;
    const usedGameId = await modelUsers.isUserPlayer(daftarGame);
    if (!usedGameId) {
      res.statusCode = 404;
      return res.json({ message: "user Id is undefined" });
    }

    modelUsers.storeUserGames(daftarGame);

    res.json({ message: "new game history is being added!" });
  };

  getOneUserHistory = async (req, res) => {
    const { userId } = req.params;
    const userHistory = await modelUsers.getGameHistories(userId);

    if (userHistory) {
      return res.json(userHistory);
    } else {
      res.statusCode = 404;
      return res.json({ message: `User id un identified : ${userId}` });
    }
  };

}

module.exports = new controlUsers();
