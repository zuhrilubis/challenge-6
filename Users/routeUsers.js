const express = require("express");
const controlUsers = require("./controlUsers");
const routeUsers = express.Router();

routeUsers.post("/daftar", controlUsers.storeNewUsers);

routeUsers.get("/user", controlUsers.getAllUsers);

routeUsers.post("/login", controlUsers.verifyLogin);

routeUsers.post("/adduserbio", controlUsers.storeUserBio);

routeUsers.put("/updatebio/:userId", controlUsers.updateUserBio);

routeUsers.get("/detiles/:userId", controlUsers.getOneUser);

routeUsers.post("/addgamehistory", controlUsers.storeGameHistory);

routeUsers.get("/gamehistory/:userId", controlUsers.getOneUserHistory);

module.exports = routeUsers;