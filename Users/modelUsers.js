const md5 = require("md5");
const db = require('../db/models');
const { Op } = require("sequelize");


const userList = [];

class modelUsers {
    getAllUsers = async () => {
        const dataUsers = await db.User.findAll();

        return dataUsers;
    };

    getOneUser = async (userId) => {
        return await db.User.findOne({
            include: [db.UserBio],
            where: { id: userId }
        });
    };

    storeUserBio = (daftarBio) => {
        db.UserBio.create({
            fullname: daftarBio.fullname,
            Address: daftarBio.Address,
            PhoneNumber: daftarBio.PhoneNumber,
            User_id: daftarBio.User_id,
        });
    };

    isBioIdentified = async (daftarBio) => {
        const usedBio = await db.UserBio.findOne({
            where: { User_id: daftarBio.User_id },
        });

        if (usedBio) {
            return true;
        } else {
            return false;
        }
    };

    updateUserBio = async (userId, fullname, Address, PhoneNumber) => {
        return await db.UserBio.update(
            { fullname: fullname, Address: Address, PhoneNumber: PhoneNumber },
            { where: { User_id: userId } }
        );
    };

    isUpdateBioUndiefined = async (userId) => {
        const updateBio = await db.UserBio.findOne({
            where: { User_id: userId },
        });

        if (updateBio) {
            return true;
        } else {
            return false;
        }
    };


    storeNewUsers = (daftarData) => {
        db.User.create({
            username: daftarData.username,
            email: daftarData.email,
            password: md5(daftarData.password),
        });
    };

    isUserIdentified = async (daftarData) => {
        const usedData = await db.User.findOne({
            where: {
                [Op.or]: [
                    { username: daftarData.username },
                    { email: daftarData.email }
                ]
            }
        });

        if (usedData) {
            return true;
        } else {
            return false;
        }
    };

    isUserUnIdentified = async (dataBio) => {
        const unUsedData = await db.User.findOne({
            where: { id: dataBio.User_id },
        });

        if (unUsedData) {
            return true;
        } else {
            return false;
        }
    };

    verifyLogin = async (username, password) => {
        const dataLogin = await db.User.findOne({
            where: { username: username, password: md5(password) },
        });

        return dataLogin;
    };

    storeUserGames = (daftarGame) => {
        db.GameHistory.create({
            User_id: daftarGame.User_id,
            status: daftarGame.status,
        });
    };

    isUserPlayer = async (daftarGame) => {
        const usedGameId = await db.User.findOne({
            where: { id: daftarGame.User_id },
        });

        if (usedGameId) {
            return true;
        } else {
            return false;
        }
    };


    getGameHistories = async (userId) => {
        return await db.User.findOne({
            include: [db.GameHistory],
            where: { id: userId },
        });
    };

}


module.exports = new modelUsers;
